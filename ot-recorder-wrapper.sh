#!/bin/sh

set -e

mkdir -p /owntracks/recorder/store/last
chown -R owntracks: /owntracks

export OTR_STORAGEDIR=/owntracks/recorder/store
export OTR_TOPICS="owntracks/#"
export OTR_CAFILE=/etc/ssl/certs/ca-certificates.crt

sudo -u owntracks /usr/sbin/ot-recorder --initialize
exec sudo -Eu owntracks /usr/sbin/ot-recorder --http-host 0.0.0.0
