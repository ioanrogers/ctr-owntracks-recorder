#!/usr/bin/make

ACBUILD=acbuild --debug
ACBUILD_PRIV=sudo acbuild --debug

all: owntracks-recorder.aci

owntracks-recorder.aci:
	$(ACBUILD) begin
	$(ACBUILD) set-name rgrs.ca/images/owntracks-recorder
	$(ACBUILD) dep add quay.io/dockerlibrary/debian:stretch-slim
	$(ACBUILD) port add http tcp 8083
	$(ACBUILD) mount add owntracks /owntracks

	$(ACBUILD_PRIV) run -- apt update
	$(ACBUILD_PRIV) run -- apt install -y gnupg2 sudo
	$(ACBUILD_PRIV) run -- apt-key adv --keyserver hkp://sks-keyservers.net --recv BE1675153E0A5116
	$(ACBUILD_PRIV) run -- /bin/sh -c "echo 'deb http://repo.owntracks.org/debian stretch main' > /etc/apt/sources.list.d/owntracks.list"
	$(ACBUILD_PRIV) run -- apt update
	$(ACBUILD_PRIV) run -- apt install -y ot-recorder
	$(ACBUILD_PRIV) run -- apt-get clean
	$(ACBUILD_PRIV) run -- rm -rf /var/lib/apt/lists/*
	$(ACBUILD_PRIV) copy ot-recorder-wrapper.sh /usr/sbin/ot-recorder-wrapper.sh

	$(ACBUILD) set-user root
	$(ACBUILD) set-exec -- /usr/sbin/ot-recorder-wrapper.sh
	$(ACBUILD_PRIV) write --overwrite owntracks-recorder.aci
	$(ACBUILD_PRIV) end

clean:
	sudo rm -rf .acbuild owntracks-recorder.aci
